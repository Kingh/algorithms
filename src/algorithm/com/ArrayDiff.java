package algorithm.com;

import java.util.Arrays;
import java.util.stream.IntStream;

public class ArrayDiff {
    public static int diff(int[] arr1, int[] arr2) {
        IntStream stream = IntStream.concat(Arrays.stream(arr1), Arrays.stream(arr2));

        return stream.reduce((a, b) -> (a ^ b)).getAsInt();
    }
}
