package algorithm.com;

public class BinarySearch {
    /**
     * Searches for {@value <T> key} in the array {@value <T> V[]}
     * @param y array of generic values
     * @param key the generic value to find in the array
     * @param <T> generic type
     * @param <V> generic type
     * @return the index of {@value <T> key} if found in the array else -1
     */
    public static <T extends Comparable<T>, V extends T> int search(V[] y, T key) {
        int lo = 0;
        int hi = y.length - 1;
        int mid = lo + (hi - lo) / 2;

        for ( ; lo <= hi; mid = lo + (hi - lo) / 2) {
            if (y[mid].compareTo(key) < 0) {
                lo = mid + 1;
            } else if (y[mid].compareTo(key) > 0) {
                hi = mid - 1;
            } else {
                return mid;
            }
        }

        return -1;
    }
}
