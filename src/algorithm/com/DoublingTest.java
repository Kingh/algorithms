package algorithm.com;

public class DoublingTest {
    public static double timeTrail(int n) {
        final int MAX = 1000000;
        int[] a = new int[n];

        for (int i = 0; i < n; i++) {
            a[i] = StdRandom.uniform(-MAX, MAX);
        }

        Stopwatch timer = new Stopwatch();
        int count = ThreeSum.count(a);

        return timer.elapsedTime();
    }
}
