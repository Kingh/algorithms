package algorithm.com;

public class EuclidAlgo {

    /**
     * Gets the greatest common divisor of two numbers
     * @param p first number
     * @param q second number
     * @return the greatest common divisor
     */
    public static int gcd(int p, int q) {
        if (q == 0) {
            return p;
        }

        int r = p % q;

        return gcd(q, r);
    }
}
