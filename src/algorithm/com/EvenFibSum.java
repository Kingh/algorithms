package algorithm.com;

import algorithm.com.exceptions.InvalidUpperBound;

public class EvenFibSum {
    public static int sum(int bound) throws InvalidUpperBound {
        if (bound < 2) {
            throw new InvalidUpperBound(bound + " is not a valid Fibonacci upper bound");
        }

        int fn_1 = 8;
        int fn_2 = 2;
        int res = fn_1 + fn_2;
        int fn = (fn_1 << 2) + fn_2;

        for (; fn < bound; ) {
            res += fn;
            fn_2 = fn_1;
            fn_1 = fn;
            fn = (fn_1 << 2) + fn_2;
        }

        return res;
    }
}
