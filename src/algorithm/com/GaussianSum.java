package algorithm.com;

import algorithm.com.exceptions.InvalidArrayLength;

public class GaussianSum {
    public static int sum(int[] range) throws InvalidArrayLength {
        if (range.length != 2) {
            throw new InvalidArrayLength("Invalid range: " + range.length);
        }

        int max = Math.max(range[0], range[1]);
        int min = Math.min(range[0], range[1]);

        return ((max - min + 1) * (max + min)) / 2;
    }
}
