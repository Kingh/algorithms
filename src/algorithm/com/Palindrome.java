package algorithm.com;

public class Palindrome {
    public static boolean isPalindrome(String palindrome) {
        int n = palindrome.length();
        int j = n - 1;

        for (int i = 0; i < n/2 ; i++, j--) {
            if (palindrome.charAt(i) != palindrome.charAt(j)) {
                return false;
            }
        }

        return true;
    }
}
