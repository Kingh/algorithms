package algorithm.com;

public class PalindromeRecursive {
    public static boolean isPalindrome(String palindrome) {
        StringBuilder buffer = new StringBuilder(palindrome);
        int len = buffer.length();

        if (len <= 1) {
            return true;
        }

        if (buffer.charAt(0) == buffer.charAt(len - 1)) {
            buffer.deleteCharAt(len - 1);
            buffer.deleteCharAt(0);
        } else {
            return false;
        }

        return isPalindrome(buffer.toString());
    }
}
