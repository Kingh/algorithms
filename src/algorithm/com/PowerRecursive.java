package algorithm.com;

public class PowerRecursive {
    public static double power(int x, int exponent) {
        if (exponent == 0) {
            return 1;
        }

        if (exponent < 0) {
            return 1 / power(x, Math.abs(exponent));
        }

        if (!isEven(exponent)) {
            return x * power(x, exponent - 1);
        }

        if (isEven(exponent)) {
            double y = power(x, exponent / 2);
            return y * y;
        }

        return 0;
    }

    private static boolean isEven(int number) {
        return ((number != 0) && (number % 2) == 0);
    }
}
