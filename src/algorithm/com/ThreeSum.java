package algorithm.com;

public class ThreeSum {
    /**
     * Count triples that sum to 0
     * @param a an array of ints
     * @return the number of triples that sum to 0
     */
    public static int count(int[] a) {
        int len = a.length;
        int count = 0;

        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len ; j++) {
                for (int k = j + 1; k < len; k++) {
                    if (a[i] + a[j] + a[k] == 0) {
                        count++;
                    }
                }
            }
        }

        return count;
    }
}
