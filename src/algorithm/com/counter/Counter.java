package algorithm.com.counter;

import java.util.Objects;

public class Counter {
    private final String id;
    private int count;

    public Counter(String id) {
        this.id = id;
    }

    public void increment() {
        count++;
    }

    public int tally() {
        return count;
    }

    @Override
    public String toString() {
        return "Counter{" +
                "name='" + id + '\'' +
                ", count=" + count +
                '}';
    }
}
