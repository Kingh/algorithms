package algorithm.com.exceptions;

public class InvalidArrayLength extends Exception {
    public InvalidArrayLength(String message) {
        super(message);
    }
}
