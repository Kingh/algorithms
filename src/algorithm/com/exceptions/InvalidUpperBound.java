package algorithm.com.exceptions;

public class InvalidUpperBound extends Exception {
    public InvalidUpperBound(String message) {
        super(message);
    }
}
