package algorithm.com.sort;

import java.util.ArrayList;
import java.util.List;

public class MergeSort {
    public static <T extends Comparable<T>> void sort(T[] array, int lo, int hi) {
        if (lo < hi) {
            int mid = ((lo + hi) / 2);

            sort(array, lo, mid);
            sort(array, mid + 1, hi);
            merge(array, lo, mid, hi);
        }
    }

    public static <T extends Comparable<T>> void merge(T[] array, int lo, int mid, int hi) {
        List<T> lowHalf = new ArrayList<>();
        List<T> highHalf = new ArrayList<>();

        int j = lo;
        for ( ; j <= mid; j++) {
            lowHalf.add(array[j]);
        }

        for ( ; j <= hi; j++) {
            highHalf.add(array[j]);
        }

        j = lo;
        int i = 0;
        int l = 0;
        int lowSize = lowHalf.size();
        int highSize = highHalf.size();

        while (i < lowSize && l < highSize) {
            T low = lowHalf.get(i);
            T high = highHalf.get(l);

            if (low.compareTo(high) < 0) {
                array[j++] = low;
                i++;
            } else {
                array[j++] = high;
                l++;
            }
        }

        while (i < lowSize) {
            array[j++] = lowHalf.get(i++);
        }

        while (l < highSize) {
            array[j++] = highHalf.get(l++);
        }
    }
}
