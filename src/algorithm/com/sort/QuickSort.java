package algorithm.com.sort;

public class QuickSort {
    public static <T extends Comparable<T>> void sort(T[] array, int lo, int hi) {
        if (lo < hi) {
            int mid = partition(array, lo, hi);

            sort(array, lo, mid - 1);
            sort(array, mid + 1, hi);
        }
    }

    private static <T extends Comparable<T>> int partition(T[] array, int lo, int hi) {
        int pivot = lo;

        for (int i = lo; i < hi; i++) {
            if (array[hi].compareTo(array[i]) > 0) {
                swap(array, i, pivot++);
            }
        }

        swap(array, hi, pivot);

        return pivot;
    }

    private static <T> void swap(T[] array, int index1, int index2) {
        T temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
}
