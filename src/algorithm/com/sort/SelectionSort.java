package algorithm.com.sort;

public class SelectionSort {
    /**
     * Sorts a generic array
     *
     * @param array to sort
     * @param <T> generic element
     */
    public static <T extends Comparable<T>> void sort(T[] array) {
        for (int i = 0; i < array.length; i++) {
            T min = array[i];
            int minIndex = i;

            for (int j = (i + 1); j < array.length; j++) {
                if (min.compareTo(array[j]) > 0) {
                    min = array[j];
                    minIndex = j;
                }
            }

            if (min != array[i]) {
                swap(array, minIndex, i);
            }
        }
    }

    /**
     * Swaps the values of the specified indexes in an array
     *
     * @param array array for which to swap values
     * @param a index of value to be swapped
     * @param b index of value to be swapped
     * @param <T> generic element
     */
    private static <T> void swap(T[] array, int a, int b) {
        T temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }
}
