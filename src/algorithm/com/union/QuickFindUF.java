package algorithm.com.union;

import java.util.Arrays;

public class QuickFindUF {
    private int[] id;

    public QuickFindUF(int N) {
        if (N <= 0) {
            throw new IllegalArgumentException("number of nodes N should be greater than zero");
        }

        id = new int[N];

        for (int i = 0; i < N; i++) {
            id[i] = i;
        }
    }

    /**
     * Check if p and q are in the same component
     *
     * @param p element in component
     * @param q element in component
     * @return
     */
    public boolean connected(int p, int q) {
        return id[p] == id[q];
    }

    /**
     * Changes all entries with {@code id[p]} to {@code id[q]}
     *
     * @param p element in component
     * @param q element in component
     */
    public void union(int p, int q) {
        int pid = id[p];
        int qid = id[q];

        for (int i = 0; i < id.length; i++) {
            if (id[i] == pid) {
                id[i] = qid;
            }
        }
    }

    @Override
    public String toString() {
        return "QuickFindUF{" +
                "id=" + Arrays.toString(id) +
                '}';
    }
}
