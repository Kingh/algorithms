package fiset.algorithms.com;

/**
 * UnionFind/Disjoint Set data structure implementation
 */
public class UnionFind {
    // The number of elements in this union find
    private int size;

    // Used to track the size of each of the components
    private int[] sz;

    // id[i] point to the parent of i, if id[i] = i then i is a root node
    private int[] id;

    // Tracks the number if components in the union find
    private int numComponents;

    public UnionFind(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("size <= 0 is not allowed");
        }

        this.size = numComponents = size;
        sz = new int[size];
        id = new int[size];

        for (int i = 0; i < size; i++) {
            id[i] = i; // Link node to itself (self root)
            sz[i] = 1; // Each component is originally of size one
        }
    }

    /**
     * Find which component/set 'p' belongs to, takes amortized constant time.
     *
     * @param p
     * @return
     */
    public int find(int p) {
        // Find the root of the componet/set
        int root = p;
        while (root != id[root]) {
            root = id[root];

            // Compress the path leading back to the root
            // Doing this operation is called "path compression"
            // and is what gives us amortized constant time complexity
            while (p != root) {
                int next = id[p];
                id[p] = root;
                p = next;
            }
        }

        return root;
    }

    /**
     * @param p
     * @param q
     * @return Return whether or not the elements 'p' and 'q' are in the same components/set
     */
    public boolean connected(int p, int q) {
        return find(p) == find(q);
    }

    /**
     * @param p
     * @return Return the size of the components/set 'p' belongs to
     */
    public int componentSize(int p) {
        return sz[find(p)];
    }

    /**
     * @return return the number of elements in this UnionFind/Disjoint set
     */
    public int size() {
        return size;
    }

    /**
     * @return Return the number of remaining components/set
     */
    public int components() {
        return numComponents;
    }

    /**
     * Unify the components/sets containing elements 'p' and 'q'
     *
     * @param p
     * @param q
     */
    public void union(int p, int q) {
        int root1 = find(p);
        int root2 = find(q);

        // These elements are already in the same set!
        if (root1 == root2) return;

        // Merge two components/sets together.
        // Merge smaller component/set into the larger one.
        if (sz[root1] < sz[root2]) {
            sz[root2] += sz[root1];
            id[root1] = root2;
        } else {
            sz[root1] += sz[root2];
            id[root2] = root1;
        }

        // Since the roots found are different we know that
        // the number of components/sets has decreased by one
        numComponents--;
    }
}
