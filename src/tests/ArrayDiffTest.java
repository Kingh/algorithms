package tests;

import algorithm.com.ArrayDiff;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArrayDiffTest {
    int[] arr1;
    int[] arr2;

    @BeforeEach
    void setUp() {
         arr1 = new int[]{1, 3, 9, 7, 5, 2, 4,6, 8};
        arr2 = new int[]{1, 3, 9, 5, 2, 4,6, 8};
    }

    @Test
    void diffTest() {
        assertEquals(7, ArrayDiff.diff(arr1, arr2));
    }
}
