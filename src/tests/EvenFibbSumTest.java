package tests;

import algorithm.com.EvenFibSum;
import algorithm.com.exceptions.InvalidUpperBound;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EvenFibbSumTest {
    @Test
    void sumTest() {
        try {
            assertEquals(188, EvenFibSum.sum(200));
        } catch (InvalidUpperBound invalidUpperBound) {
            invalidUpperBound.printStackTrace();
        }
    }
}
