package tests;

import algorithm.com.sort.InsertionSort;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertSame;

class InsertionSortTest {
    Integer nums[];
    Integer twoElemNums[];
    Integer oneElemNums[];
    Integer sortedNums[];
    String strings[];
    String twoElemStrings[];
    String sortedStrings[];
    String oneElemStrings[];

    @BeforeEach void setUp() {
        nums = new Integer[]{11, 5, 10, 0, 6, 8, 2, 7, 3, 4, 9};
        oneElemNums = new Integer[]{11};
        twoElemNums = new Integer[]{10, 11};
        strings = new String[]{"a", "f", "g", "b", "d", "c", "t", "z", "l", "e", "h"};
        sortedNums = new Integer[]{1, 2, 3, 4, 5};
        sortedStrings = new String[]{"a", "b", "c", "d"};
        oneElemStrings = new String[]{"a"};
        twoElemStrings = new String[]{"b", "a"};
    }

    @AfterEach void setDown() {
        nums = null;
    }

    @Test
     void testNormalArray() {
        InsertionSort.sort(nums);
        assertSame(Arrays.equals(nums, new Integer[]{0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}), true);
        InsertionSort.sort(strings);
        assertSame(Arrays.equals(strings, new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "l", "t", "z"}), true);
    }

    @Test
    void testSortedArray() {
        InsertionSort.sort(sortedNums);
        assertSame(Arrays.equals(sortedNums, new Integer[]{1, 2, 3, 4, 5}), true);
        InsertionSort.sort(sortedStrings);
        assertSame(Arrays.equals(sortedStrings, new String[]{"a", "b", "c", "d"}), true);
    }

    @Test
    void testArrayWithOneElement() {
        InsertionSort.sort(oneElemNums);
        assertSame(Arrays.equals(oneElemNums, new Integer[]{11}), true);
        InsertionSort.sort(oneElemStrings);
        assertSame(Arrays.equals(oneElemStrings, new String[]{"a"}), true);
    }

    @Test
    void testArrayWithTwoElements() {
        InsertionSort.sort(twoElemNums);
        assertSame(Arrays.equals(twoElemNums, new Integer[]{10, 11}), true);
        InsertionSort.sort(twoElemStrings);
        assertSame(Arrays.equals(twoElemStrings, new String[]{"a", "b"}), true);
    }
}