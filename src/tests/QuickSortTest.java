package tests;

import algorithm.com.sort.MergeSort;
import algorithm.com.sort.QuickSort;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertSame;

class QuickSortTest {
    private Integer[] nums;
    private Integer[] oneElemNums;
    private Integer[] sortedNums;
    private String[] strings;
    private String[] sortedStrings;
    private String[] oneElemStrings;

    @BeforeEach void setUp() {
        nums = new Integer[]{11, 5, 10, 1, 6, 8, 2, 7, 3, 4, 9};
        oneElemNums = new Integer[]{11};
        strings = new String[]{"a", "f", "g", "b", "d", "c", "t", "z", "l", "e", "x"};
        sortedNums = new Integer[]{1, 2, 3, 4, 5};
        sortedStrings = new String[]{"a", "b", "c", "d"};
        oneElemStrings = new String[]{"a"};
    }

    @AfterEach void setDown() {
        nums = null;
    }

    @Test
    void testNormalArray() {
        QuickSort.sort(nums, 0, nums.length - 1);
        assertSame(Arrays.equals(nums, new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}), true);
        QuickSort.sort(strings, 0, strings.length - 1);
        assertSame(Arrays.equals(strings, new String[]{"a", "b", "c", "d", "e", "f", "g", "l", "t", "x", "z"}), true);
    }

    @Test
    void testSortedArray() {
        QuickSort.sort(sortedNums, 0, sortedNums.length - 1);
        assertSame(Arrays.equals(sortedNums, new Integer[]{1, 2, 3, 4, 5}), true);
        QuickSort.sort(sortedStrings, 0, sortedStrings.length - 1);
        assertSame(Arrays.equals(sortedStrings, new String[]{"a", "b", "c", "d"}), true);
    }

    @Test
    void testArrayWithOneElement() {
        QuickSort.sort(oneElemNums, 0, oneElemNums.length - 1);
        assertSame(Arrays.equals(oneElemNums, new Integer[]{11}), true);
        QuickSort.sort(oneElemStrings, 0, oneElemStrings.length - 1);
        assertSame(Arrays.equals(oneElemStrings, new String[]{"a"}), true);
    }
}