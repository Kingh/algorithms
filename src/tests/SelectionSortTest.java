package tests;

import algorithm.com.sort.SelectionSort;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class SelectionSortTest {
    Integer nums[];
    Integer oneElemNums[];
    Integer sortedNums[];
    String strings[];
    String sortedStrings[];
    String oneElemStrings[];

    @BeforeEach void setUp() {
        nums = new Integer[]{11, 5, 10, 1, 6, 8, 2, 7, 3, 4, 9};
        oneElemNums = new Integer[]{11};
        strings = new String[]{"a", "f", "g", "b", "d", "c", "t", "z", "l", "e", "x"};
        sortedNums = new Integer[]{1, 2, 3, 4, 5};
        sortedStrings = new String[]{"a", "b", "c", "d"};
        oneElemStrings = new String[]{"a"};
    }

    @AfterEach void setDown() {
        nums = null;
    }

    @Test
    void testNormalArray() {
        SelectionSort.sort(nums);
        assertSame(Arrays.equals(nums, new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}), true);
        SelectionSort.sort(strings);
        assertSame(Arrays.equals(strings, new String[]{"a", "b", "c", "d", "e", "f", "g", "l", "t", "x", "z"}), true);
    }

    @Test
    void testSortedArray() {
        SelectionSort.sort(sortedNums);
        assertSame(Arrays.equals(sortedNums, new Integer[]{1, 2, 3, 4, 5}), true);
        SelectionSort.sort(sortedStrings);
        assertSame(Arrays.equals(sortedStrings, new String[]{"a", "b", "c", "d"}), true);
    }

    @Test
    void testArrayWithOneElement() {
        SelectionSort.sort(oneElemNums);
        assertSame(Arrays.equals(oneElemNums, new Integer[]{11}), true);
        SelectionSort.sort(sortedStrings);
        assertSame(Arrays.equals(oneElemStrings, new String[]{"a"}), true);
    }
}